from sklearn.cluster import DBSCAN
from sklearn.neighbors import NearestNeighbors
from kneed import KneeLocator
import numpy as np
import pandas as pd
import time
import json

def find_epsilon_parameter_for_DBSCAN(MiniPoints,dataset):

	neighbors = NearestNeighbors(n_neighbors=MiniPoints)
	neighbors_fit = neighbors.fit(dataset)
	distances, indices = neighbors_fit.kneighbors(dataset)
	distances = np.sort(distances, axis=0)
	distances = distances[:,1]
	
	x_values_as_list=[]
	for i in range(len(distances)):
		x_values_as_list.append(i)
	#endfor

	x_values_numpy = np.array(x_values_as_list)
	y_values_numpy = np.array(distances)

	kneedle = KneeLocator(x_values_numpy, y_values_numpy, S=1.0, curve="convex", direction="increasing")
	epsilon_val=round(kneedle.knee_y, 3)

	return epsilon_val
#enddef

def find_MiniPts_parameter_for_DBSCAN(dimension_of_features):
	if(dimension_of_features==2):
		MinPts=4
	#endif
	else:
		MinPts=dimension_of_features*2
	#endelse
	return MinPts
#enddef

def find_dbscan_params(training_set):

	MiniPts = find_MiniPts_parameter_for_DBSCAN(len(training_set[0]))
	epsilon = find_epsilon_parameter_for_DBSCAN(MiniPts,training_set)

	return MiniPts,epsilon
#enddef

def DBSCAN_Algorithm(input_data,epsilon,MiniPoints):
	
	training_data = input_data
	
	start_time_training = time.time()
    
	ClustersInfoDBSCAN = DBSCAN(eps = epsilon, min_samples = MiniPoints)
	ClustersInfoDBSCAN.fit(training_data)

	end_time_training = time.time()
    
	print("Total training time DBSCAN", end_time_training-start_time_training)

	core_points_of_the_clusters = ClustersInfoDBSCAN.components_
	indexes_of_the_core_ponts_in_the_dataset = ClustersInfoDBSCAN.core_sample_indices_
	ids_of_clusters_and_noise = np.unique(ClustersInfoDBSCAN.labels_)

	unique_ids_of_clusters = []

	for i in range(len(ids_of_clusters_and_noise)):
		if(ids_of_clusters_and_noise[i]!=-1):
			unique_ids_of_clusters.append(ids_of_clusters_and_noise[i])
		#endif
	#endfor
	number_of_unique_clusters=len(unique_ids_of_clusters)

	clusters_with_their_id_and_core_points={}

	for i in range(number_of_unique_clusters):
		members_of_each_cluster=[]
		for j in range(len(training_data)):
			if(ClustersInfoDBSCAN.labels_[j]==i):
				members_of_each_cluster.append(training_data[j])
			#endif
		#endfor
		clusters_with_their_id_and_core_points[str(i)]=members_of_each_cluster
	#endfor
	return unique_ids_of_clusters, core_points_of_the_clusters, clusters_with_their_id_and_core_points
#enddef

def find_outliers(cluster_cores, test_set, epsilon):
	outlier = []
	distances = []
	for sample in test_set:
		distance, decision = outliers_detection(cluster_cores, sample, epsilon)
		outlier.append(decision)
		distances.append(distance)
	#endfor
	return outlier, distances
#enddef
def outliers_detection(cluster_cores, sample, epsilon):
    # Calculate the distance from new input data to all core points of created cluster
    dist = np.linalg.norm(cluster_cores - sample, axis=1)

    # Select the minimum distance
    min_dist = np.min(dist)

    # Compare the distance to the closest neighbor inside the cluster with epsilon parameter (ε) of DBSCAN
    if min_dist > epsilon:
        return min_dist, "True" # is an outlier
    #endif
    else:
        return min_dist, "False" # is NOT an outlier
    #endelse
#enddef

def make_the_Dataset_from_json(tc_list,metrics):
	
	in_data = {}
	KPI = []

	for test_case in tc_list: # tcs[test_case] edw exoume lista me lexika ara edw h metavlhth test_case mou dinei to kathe lexiko vector
		for vector in test_case:
			for node in test_case[vector]: # pairnoyme san value me key ena sygekrimeno vector info gia sygekrimeno node (VM as poume estw A)               
				metric_list = []
				for metric in metrics:                
					try:
						metric_list.append(test_case[vector][node][metric]["metric_value"]) # ara edw ftanoume emfwleymena mesa apo ta dictionaries na paroyme opoio metric theloume kai to metric value pou exei
					except KeyError:
						KPI.append(metric) if metric not in KPI else KPI
						metric_list.append(test_case[vector][node][metric]["kpi_value"])
                #endfor # finish with the acquisition of the metrics
                #in_data.update({vector+" | "+node+" | "+format(cnt):metric_list})
				in_data.update({vector+" | "+node+" | "+format(time.time()):metric_list})
            #endfor
        #endfor
    #endfor
    # new data of this experiment are prepared to be fed to DBSCAN
	in_data_df = pd.DataFrame.from_dict(in_data, orient='index', columns=metrics) 

	in_data_df_nn = in_data_df.dropna() # drop missing values

	in_data_df_round = in_data_df_nn.round(0) # stroggylopoihsh sto mhden
    
	in_data = in_data_df_round.values # list of listes ready for DBSCAN

	return in_data
#endef

def detect_if_outlier(sample,cluster_cores,epsilon_val,metrics):
	# Calculate the distance from new input data to all core points of created cluster
	dist = np.linalg.norm(cluster_cores - sample, axis=1)
	# Select the minimum distance
	min_dist = np.min(dist)
	# Compare the distance to the closest neighbor inside the cluster with epsilon parameter (ε) of DBSCAN
	if min_dist > epsilon_val:
		for i in range(len(cluster_cores)):
			distance_from_core_pnt = np.linalg.norm(cluster_cores[i]-sample,axis=1)
			if(distance_from_core_pnt==min_dist):
				perc_metric_change={}
				for j in range(len(metrics)):
					diff_val_in_the_metric = sample[0][j] - cluster_cores[i][j]
					perc_metric_change[metrics[j]]=diff_val_in_the_metric
				#endfor
				break
			#endif
		#endfor
		return "True", perc_metric_change # is an outlier
	#endif
	else:
		for i in range(len(cluster_cores)):
			distance_from_core_pnt = np.linalg.norm(cluster_cores[i]-sample,axis=1)
			if(distance_from_core_pnt==min_dist):
				perc_metric_change={}
				for j in range(len(metrics)):
					diff_val_in_the_metric = sample[0][j] - cluster_cores[i][j]
					perc_metric_change[metrics[j]]=diff_val_in_the_metric
				#endfor
				break
			#endif
		#endfor
		return "False", perc_metric_change # is NOT an outlier
	#endelse
#enddef

def create_out_json_with_outliers_info(data_from_json_file,metrics,corepoints,epsilon):
	
	for tcs in data_from_json_file["testcases"]:
		for test_case in tcs:
			for case in tcs[test_case]:
				for vector in case:
					for node in case[vector]:
						metric_list = []
						for metric in metrics:
							try:
								metric_list.append(case[vector][node][metric]["metric_value"])
							except KeyError:
								metric_list.append(case[vector][node][metric]["kpi_value"])
			            #endfor
						data = {vector+" | "+node:metric_list}
						df = pd.DataFrame.from_dict(data, orient='index', columns=metrics)
						df_not_nan = df.dropna()
						df_round = df_not_nan.round(0)
						specific_data = df_round.values
						#print(data)
						#exit(0)
			            #outliers detection 
						true_or_false, perc_metric_change = detect_if_outlier(specific_data,corepoints,eps,metrics)
						case[vector][node].update({"outlier" : true_or_false,"perc_metric_change" : perc_metric_change})
						#print("=======================================================")
						#print(case)
						#print("=======================================================")
						#test_case[vector][node].update({"outlier" : str(is_outlier[0]),"distance" : distance, "threshold": threshold, "win_neuron": winner, "metric" : metrics[metric_index[0]], "perc_metric_change":{}})
						#for i in range(len(metrics)):
							#test_case[vector][node]["perc_metric_change"].update({metrics[i] : perc_metric_change[0][i]})
						#endfor
					#endfor
				#endfor
			#endfor
		out = json.dumps(tcs[test_case], indent = 4)
		with open("test_case_1_out_from_DBSCAN.json","w") as file:
			file.write(out)		
		#endfor
	#endfor
	file.close()
#enddef

# main() #
"""
dataset=[[1,2],[3,4],[2.5,4],[1.5,2.5],[3,5],[2.8,4.5],[2.5,4.5],[1.2,2.5],[1,3],[1,5],[1,2.5],[5,6],[4,3]]
"""
try: 
	with open("test.json","r") as file:
		data_from_json = json.load(file)
	#endwith
#end-try
except FileNotFoundError:  
		response = "File not found!"
		print(response)
#end_except

print(data_from_json)
for tcs in data_from_json["testcases"]:
	for test_case in tcs:
		metrics= ["cpu","memory","disk","average_resources"]
		fixed_dataset = make_the_Dataset_from_json(tcs[test_case],metrics)
	#endfor
#endfor

minipts,eps = find_dbscan_params(fixed_dataset)

list_with_unique_cluster_ids, core_pts, DictClustersWithPoints = DBSCAN_Algorithm(fixed_dataset,eps,minipts)

#outliers, distances = find_outliers(core_pts,fixed_dataset,eps)

create_out_json_with_outliers_info(data_from_json,metrics,core_pts,eps)

#endmain



"""
clusters_with_DBSCAN = DBSCAN(eps=eps, min_samples=minipts)
clusters_with_DBSCAN.fit(dataset)

for i in range(len(dataset)):
	if(clusters_with_DBSCAN.labels_[i]==-1):
		print("("+str(dataset[i][0])+","+str(dataset[i][1])+") is an outlier")
	#endif
#endfor
"""

